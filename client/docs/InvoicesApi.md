# InvoicesApi

All URIs are relative to *http://localhost:8080/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addInvoice**](InvoicesApi.md#addInvoice) | **POST** /invoices | Create a new invoice
[**searchInvoices**](InvoicesApi.md#searchInvoices) | **GET** /invoices | Returns a list of invoices




<a name="addInvoice"></a>
# **addInvoice**
> Invoice addInvoice()

Create a new invoice

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.InvoicesApi;



InvoicesApi apiInstance = new InvoicesApi();

try {
    Invoice result = apiInstance.addInvoice();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling InvoicesApi#addInvoice");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.


### Return type

[**Invoice**](Invoice.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


<a name="searchInvoices"></a>
# **searchInvoices**
> Invoices searchInvoices()

Returns a list of invoices

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.InvoicesApi;



InvoicesApi apiInstance = new InvoicesApi();

try {
    Invoices result = apiInstance.searchInvoices();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling InvoicesApi#searchInvoices");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.


### Return type

[**Invoices**](Invoices.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



