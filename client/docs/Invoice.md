
# Invoice

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**invoiceNumber** | **String** |  | 
**poNumber** | **String** |  | 
**dueDate** | [**LocalDate**](LocalDate.md) |  | 
**amountCents** | **Long** |  | 




