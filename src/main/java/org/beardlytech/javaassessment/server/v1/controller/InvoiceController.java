package org.beardlytech.javaassessment.server.v1.controller;

import java.util.Optional;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.beardlytech.javaassessment.server.v1.lib.OffsetLimitPageable;
import org.beardlytech.javaassessment.server.v1.model.Invoice;
import org.beardlytech.javaassessment.server.v1.repository.InvoiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@RestController
public class InvoiceController {

    private final InvoiceRepository invoiceRepository;

    @Autowired
    InvoiceController(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }

    @RequestMapping(path = "/v1/invoices", method = RequestMethod.GET, produces = "application/json")
    Page<Invoice> searchInvoices(
        @RequestParam(name="limit", required=false, defaultValue="10") int limit,
        @RequestParam(name="offset", required=false, defaultValue="0") long offset,
        @RequestParam("invoice_number") Optional<String> invoiceNumber,
        @RequestParam("po_number") Optional<String> poNumber
    ) {
        Sort sort = new Sort(Sort.Direction.DESC, "createdAt");
        Pageable pageable = new OffsetLimitPageable(
            offset,
            limit,
            sort
        );

        if (invoiceNumber.isPresent() && poNumber.isPresent()) {
            return this.invoiceRepository.findAllByInvoiceNumberContainingAndPoNumberContaining(invoiceNumber, poNumber, pageable);
        }

        if (invoiceNumber.isPresent()) {
            return this.invoiceRepository.findAllByInvoiceNumberContaining(invoiceNumber, pageable);
        }

        if (poNumber.isPresent()) {
            return this.invoiceRepository.findAllByPoNumberContaining(poNumber, pageable);
        }

        return this.invoiceRepository.findAll(pageable);
    }

    @RequestMapping(path = "/v1/invoices", method = RequestMethod.POST, produces = "application/json")
    ResponseEntity<?> addInvoice(@RequestBody Invoice input) {
        try {
            Invoice result = this.invoiceRepository.save(
                new Invoice(input.getInvoiceNumber(),
                    input.getPoNumber(), 
                    input.getDueDate(), 
                    input.getAmountCents()));

            HttpHeaders responseHeaders = new HttpHeaders();

            return new ResponseEntity<Invoice>(result, responseHeaders, HttpStatus.CREATED);
            
        } catch(Exception e) {
            System.out.println("Error when attempting to create new invoice");
            return ResponseEntity.noContent().build();
        }
    }
}