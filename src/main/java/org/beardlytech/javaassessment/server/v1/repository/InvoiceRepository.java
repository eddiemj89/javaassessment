package org.beardlytech.javaassessment.server.v1.repository;

import java.util.Optional;

import org.beardlytech.javaassessment.server.v1.model.Invoice;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface InvoiceRepository extends PagingAndSortingRepository<Invoice, Long> {
    Page<Invoice> findAllByInvoiceNumberContaining(Optional<String> invoice_number, Pageable pageable);
    Page<Invoice> findAllByPoNumberContaining(Optional<String> po_number, Pageable pageable);
    Page<Invoice> findAllByInvoiceNumberContainingAndPoNumberContaining(Optional<String> invoice_number, Optional<String> po_number, Pageable pageable);
    Invoice findByInvoiceNumber(String invoice_number);
    Invoice findByInvoiceNumberAndPoNumber(String invoice_number, String po_number);
    Invoice findByPoNumber(String po_number);
}