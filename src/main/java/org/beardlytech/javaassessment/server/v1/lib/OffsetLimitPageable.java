package org.beardlytech.javaassessment.server.v1.lib;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

// By default Pageable's constructor takes a page number and a size 
//  and calculates offset internally
// OffsetLimitPageable instead directly defines the limit & offset
public class OffsetLimitPageable implements Pageable {
    private int limit;
    private long offset;
    private Sort sort;

    public OffsetLimitPageable(long offset, int limit, Sort sort){
        this.limit = limit;
        this.offset = offset;
        this.sort = sort;
    }

    @Override
    public int getPageNumber() {
        return 0;
    }

    @Override
    public int getPageSize() {
        return limit;
    }

    @Override
    public long getOffset() {
        return offset;
    }

    @Override
    public Sort getSort() {
        return sort;
    }

    @Override
    public Pageable next() {
        long newOffset = offset + limit;
        return new OffsetLimitPageable(newOffset, limit, sort);
    }

    @Override
    public boolean hasPrevious() {
        if (offset > 0) {
            return true;
        }
        
        return false;
    }

    @Override
    public Pageable previousOrFirst() {
        long newOffset = limit - offset;

        if (newOffset < 0) {
            newOffset = 0;
        }

        return new OffsetLimitPageable(newOffset, limit, sort);
    }

    @Override
    public Pageable first() {
        long newOffset = 0;
        return new OffsetLimitPageable(newOffset, limit, sort);
    }
}