package org.beardlytech.javaassessment.server.v1.model;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

import com.fasterxml.jackson.annotation.JsonFormat;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GenerationType;

@Entity
@Table(name="invoices")
public class Invoice {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @ApiModelProperty(hidden = true)
    private long id;

    @JsonProperty("invoice_number")
    private String invoiceNumber;

    @JsonProperty("po_number")
    private String poNumber;

    @JsonFormat(pattern="yyyy-MM-dd")
    @JsonProperty("due_date")
    private Date dueDate;

    @JsonProperty("amount_cents")
    private long amountCents;

    @JsonFormat(pattern="yyyy-MM-dd'T'HH:MM:ss'Z'")
    @JsonProperty("created_at")
    @ApiModelProperty(hidden = true)
    private Date createdAt;

    private Invoice() { }

    public Invoice(String invoice_number, String po_number, Date due_date, long amount_cents) {
        this.invoiceNumber = invoice_number;
        this.poNumber = po_number;
        this.dueDate = due_date;
        this.amountCents = amount_cents;
        this.createdAt = new Date();
    }

    public Invoice(String invoice_number, String po_number, Date due_date, long amount_cents, Date created_at) {
        this.invoiceNumber = invoice_number;
        this.poNumber = po_number;
        this.amountCents = amount_cents;
        this.dueDate = due_date;
        this.createdAt = created_at;
    }

    public long getId() {
        return id;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public String getPoNumber() {
        return poNumber;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public long getAmountCents() {
        return amountCents;
    }
    
    public Date getCreatedAt() {
        return createdAt;
    }
}