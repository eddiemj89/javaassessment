# Invoice API

This API allows retrieval and creation of invoice objects. 

It connects to a local database, and utilizes Spring Boot & Data to facilitate the REST API.

The project also utilizes Flyway for database migrations. Flyway can automagically migrate the database on project start if configured to do so.

# Prerequisites:

In order to run this project, you must have Java 8 installed. You can install either of the following JDK's:
- (Oracle JDK)[http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html]
- (OpenJDK)[https://developers.redhat.com/products/openjdk/overview/]


# Running the Application

To run the server, simply run `./gradlew bootRun` from the root directory. 

This will ensure all dependencies are installed, and start the API.

After the API is running, the server can be found at `http://localhost:8080`.

The documentation can be accessed at `http://localhost:8080/swagger-ui.html`.

# Migrations - Flyway

Flyway is used to control migrations, as it allows better control over dialect-specific database migrations. Flyway can automatically migrate the database on application start, but by default the project won't automatically migrate in cases where the database tables already exist, and to prevent new tables from being created by flyway when unnecessary. 

To enable automatic migration, update `/src/resources/application.properties`, under `spring: flyway`, and set `enabled: false` to `enabled: true`.

To manually run a migration, simply run `./gradlew flywayMigrate -i`

Other Flyway gradle commands can be found at (Flyway's Gradle Documentation)[https://flywaydb.org/documentation/gradle/]

# Swagger

Swagger UI is used to show and test the API, and can be accessed at `http://localhost:8080/swagger-ui.html`.

Springfox's Swagger integrations are used to test the endpoints. I wasn't sure how to configure Swagger's documentation to hide properties on POST, but show them on GET, so the `id` and `created_at` properties aren't shown in the example model.

# Swagger Codegen Client

A Java client library has been generated using swagger-codegen along with an OpenAPI 3.0 spec of the Invoices API, and can be found under the `client` folder.